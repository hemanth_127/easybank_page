let navigation = document.getElementById('navigation')
let hamburger_icon = document.getElementById('hamburger_icon')
let hamburger_cancel = document.getElementById('hamburger_cancel')
let mockups = document.getElementById('mockups')

document.getElementById('hamburger').onclick = function () {
  if (navigation.style.display == 'none') {
    navigation.style.display = 'flex'
    hamburger_icon.style.display = 'none'
    hamburger_cancel.style.display = 'block'
    mockups.style.opacity = '1'
  } else {
    navigation.style.display = 'none'
    hamburger_icon.style.display = 'block'
    hamburger_cancel.style.display = 'none'
    mockups.style.opacity = '1'
  }
}
